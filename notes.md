I tried to get back to an IRC interface to Slack using Matrix, and it
had some problems. Thanks to Colin Watson’s comment on that post, I
tried Daniel Beer’s slirc, and so far it seems to be working pretty
well.

Here’s what I did:

Get a Slack legacy token which slirc will use to connect to Slack as
you. Follow the instructions given at that link, and you should end up
with a token that looks something like “abcd-123456768-ETC-ETC”. Keep
a note of it.

Install the prerequisites for slirc, and download it:

sudo cpan AnyEvent AnyEvent::HTTP AnyEvent::Socket AnyEvent::WebSocket::Client URI::Encode Data::Dumper JSON
mkdir slirc
cd slirc
wget -q 'https://www.dlbeer.co.nz/articles/slirc/slirc-20180515.pl'
chmod +x slirc-20180515.pl

Create a file in the slirc directory you created above, called
rc.conf, and make it look like this:

slack_token=abcd-123456768-ETC-ETC
password=somepassword
port=6667

Replace “abcd-123456768-ETC-ETC” with the Slack legacy token you noted
down earlier.

Replace “somepassword” with something you’ve made up (not your Slack
password) – this is what you will type as the password in your IRC
client.

Run slirc and leave it running:

./slirc-20180515.pl rc.conf
(Make sure you are inside the slirc dir when you run that.)

Start your IRC client (e.g. HexChat) and add a server with address
“localhost” and port 6667, with your slack username and the password
you added in the rc.conf (which you wrote instead of “somepassword”).

This mostly works for me, except it has a tendency to open a load of
ad-hoc chats as channels, so I have to close them all to get a usable
list.
